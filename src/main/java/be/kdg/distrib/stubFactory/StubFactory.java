package be.kdg.distrib.stubFactory;

import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class<?> clazz, String address, int port) {
        StubInvocationHandler handler = new StubInvocationHandler(address, port);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{clazz}, handler);
    }
}
