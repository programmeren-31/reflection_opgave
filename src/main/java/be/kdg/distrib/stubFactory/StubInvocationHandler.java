package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class StubInvocationHandler implements InvocationHandler {
    private final List<Class<?>> parameterTypes = Arrays.asList(String.class, Integer.class, Double.class,
            Boolean.class, Character.class, int.class, boolean.class, char.class, double.class);
    private final NetworkAddress address;
    private final MessageManager messageManager;

    public StubInvocationHandler(String address, int port) {
        this.address = new NetworkAddress(address, port);
        this.messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        messageManager.send(createMethodCallMessage(method.getName(), args), address);

        Class<?> methodReturnType = method.getReturnType();

        if (!void.class.equals(methodReturnType)) {
            MethodCallMessage message = messageManager.wReceive();
            System.out.println("Received message: " + message);

            String result = message.getParameter("result");
            if (parameterTypes.contains(methodReturnType)) {
                    return parseToCorrectObject(methodReturnType, result);
            } else {
                Map<String, String> parameters = message.getParametersStartingWith("result.");
                Object instance = methodReturnType.getDeclaredConstructor().newInstance();

                for (Map.Entry<String, String> parameterEntry : parameters.entrySet()) {
                    Field field = methodReturnType.getDeclaredField(parameterEntry.getKey().split("\\.")[1]);
                    field.setAccessible(true);
                    field.set(instance, parseToCorrectObject(field.getType(), parameterEntry.getValue()));
                }
//                for (Field field : methodReturnType.getDeclaredFields()) {
////                    field.setAccessible(true);
////                    String parameterName = "result." + field.getName();
////                    String paramValue = parameters.get(parameterName);
////                    Object value = parseToCorrectObject(field.getType(), paramValue);
////                    field.set(instance, value);
////                }

                return instance;
            }
        }
        System.out.println("Returning nothing");
        return null;
    }
    
    private Object parseToCorrectObject(Class<?> returnType, String value) {
        if (String.class.equals(returnType)) {
            System.out.println("Returning string");
            return value;
        } else if(Integer.class.equals(returnType) || int.class.equals(returnType)) {
            System.out.println("Returning integer");
            return Integer.parseInt(value);
        } else if (char.class.equals(returnType)) {
            return value.charAt(0);
        } else if (boolean.class.equals(returnType)) {
            return Boolean.parseBoolean(value);
        }

        return null;
    }

    private MethodCallMessage createMethodCallMessage(String method, Object... args){
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), method);
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                final String arg = "arg" + i;
                if (parameterTypes.contains(args[i].getClass())) {
                    message.setParameter(arg, args[i].toString());
                } else {
                    Object obj = args[i];
                    Field[] declaredFields = obj.getClass().getDeclaredFields();
                    for (Field field : declaredFields) {
                        try {
                            field.setAccessible(true);
                            Object fieldValue = field.get(obj);
                            message.setParameter(arg + "." + field.getName(), fieldValue.toString());
                        } catch (IllegalAccessException e) {
                            System.err.println(e.getMessage());
                        }
                    }
                }
            }
        }


        return message;
    }

}
