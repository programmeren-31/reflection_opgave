package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author Shawn
 */
public class SkeletonImpl implements Skeleton {
    private final MessageManager messageManager;
    private final Object implementation;
    private final List<Class<?>> parameterTypes = Arrays.asList(String.class, Integer.class, Double.class,
            Boolean.class, Character.class, int.class, boolean.class, char.class, double.class);

    public SkeletonImpl(Object implementation) {
        messageManager = new MessageManager();
        this.implementation = implementation;
    }


    @Override
    public void run() {
        Thread thread = new Thread(() -> {
            while (true) {
                MethodCallMessage methodCallMessage = messageManager.wReceive();
                System.out.println(methodCallMessage);
                long l = System.currentTimeMillis();
                handleRequest(methodCallMessage);
                System.out.println(System.currentTimeMillis() - l);
            }
        });
        thread.start();

    }

    @Override
    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    @Override
    public void handleRequest(MethodCallMessage message) {
        Optional<Method> first =
                Arrays.stream(implementation.getClass().getDeclaredMethods())
                        .filter(m -> m.getName().equals(message.getMethodName())).findFirst();

        if (message.getParameters().size() != message.getParametersStartingWith("arg").size()) {
            throw new RuntimeException("Parameter name format wrong");
        }

        if (first.isEmpty()) throw new RuntimeException("Couldn't find method");

        Method method = first.get();
        int sizeOfMessageParameters = (int) message.getParameters().keySet().stream()
                .map(k -> k.split("\\.")[0]).distinct().count();

        int sizeOfMethodParameters = method.getParameterCount();

        if (sizeOfMessageParameters != sizeOfMethodParameters)
            throw new RuntimeException("Parameter count don't match");

        try {
            Object returnedObject = getObjectToReturn(message, method, sizeOfMessageParameters, sizeOfMethodParameters);
            if (returnedObject != null) {
                MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "result");
                setMethodCallParameter(returnedObject, methodCallMessage);
                messageManager.send(methodCallMessage, message.getOriginator());
            } else {
                sendEmptyReply(message.getOriginator());
            }
        } catch (NoSuchMethodException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    private Object getObjectToReturn(MethodCallMessage message, Method method, int sizeOfMessageParameters, int sizeOfMethodParameters) throws NoSuchMethodException, InstantiationException {
        try {
            if (sizeOfMethodParameters > 0) {
                Object[] newParameters = createInstanceOfParameterType(message, method, sizeOfMessageParameters, sizeOfMethodParameters);
                return method.invoke(implementation, newParameters);
            } else {
                return method.invoke(implementation);
            }

        } catch (IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setMethodCallParameter(Object returnedObject, MethodCallMessage methodCallMessage) {
        if (parameterTypes.contains(returnedObject.getClass())) {
            methodCallMessage.setParameter("result", returnedObject.toString());
        } else {
            Field[] declaredFields = returnedObject.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                field.setAccessible(true);
                try {
                    methodCallMessage.setParameter("result." + field.getName(), field.get(returnedObject).toString());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Object[] createInstanceOfParameterType(MethodCallMessage message, Method method, int sizeOfMethocCallParams, int sizeOfMethodParams) throws IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InstantiationException, InvocationTargetException {
        Object[] parameterArray = new Object[sizeOfMethodParams];
        for (int i = 0; i < sizeOfMethocCallParams; i++) {
            Map<String, String> parametersStartingWith = message.getParametersStartingWith("arg" + i);
            Object object;
            Class<?> returnType = method.getParameterTypes()[i];
            if (!parameterTypes.contains(returnType)) {
                object = parseToComplexType(returnType, parametersStartingWith);
            } else {
                object = parseToDefaultType(method.getParameterTypes()[i], parametersStartingWith.get("arg" + i));
            }
            parameterArray[i] = object;
        }
        return parameterArray;
    }

    private Object parseToComplexType(Class<?> type, Map<String, String> parameterMap) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Object instance = type.getDeclaredConstructor().newInstance();

        for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
            String paramName = entry.getKey().split("\\.")[1];
            Field field = type.getDeclaredField(paramName);
            if (field != null) {
                field.setAccessible(true);
                field.set(instance, parseToDefaultType(field.getType(), entry.getValue()));
            }
        }
        return instance;
    }

    private Object parseToDefaultType(Class<?> returnType, String value) {
        if (String.class.equals(returnType)) {
            return value;
        } else if (Integer.class.equals(returnType) || int.class.equals(returnType)) {
            return Integer.parseInt(value);
        } else if (char.class.equals(returnType)) {
            return value.charAt(0);
        } else if (boolean.class.equals(returnType)) {
            return Boolean.parseBoolean(value);
        } else if (double.class.equals(returnType)) {
            return Double.parseDouble(value);
        }
        return null;
    }

    private void sendEmptyReply(NetworkAddress address) {
        System.out.println("Sending empty reply");
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "result");
        methodCallMessage.setParameter("result", "Ok");
        messageManager.send(methodCallMessage, address);
    }
}
