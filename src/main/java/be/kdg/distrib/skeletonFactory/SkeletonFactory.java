package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.Proxy;

public class SkeletonFactory {
    public static Object createSkeleton(Object implementation) {
//        SkeletonInvocationHandler handler = new SkeletonInvocationHandler(implementation);
//        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{Skeleton.class}, handler);
        return new SkeletonImpl(implementation);
    }
}
