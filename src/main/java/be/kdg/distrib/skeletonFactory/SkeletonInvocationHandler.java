package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SkeletonInvocationHandler implements InvocationHandler, Runnable {
    private final MessageManager messageManager;
    private final List<Class<?>> parameterTypes = Arrays.asList(String.class, Integer.class, Double.class,
            Boolean.class, Character.class, int.class, boolean.class, char.class, double.class);
    private final Object implementation;

    public SkeletonInvocationHandler(Object implementation) {
        this.implementation = implementation;
        this.messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName());

        if (args != null && args.length == 1) {
            if (MethodCallMessage.class.equals(args[0].getClass())) {
                final MethodCallMessage message = (MethodCallMessage) args[0];
                long timeMillis = System.currentTimeMillis();
                handleRequest(message);
                System.out.println(System.currentTimeMillis() - timeMillis);
                //sendEmptyReply(message.getOriginator());
            }
        }

        Class<?> returnType = method.getReturnType();

        if (NetworkAddress.class.equals(returnType)) {
            return messageManager.getMyAddress();
        } else if (void.class.equals(returnType)) {
            Thread t = new Thread(this);
            t.start();
        }
        return null;
    }

    private void handleRequest(MethodCallMessage message) throws IllegalAccessException, InvocationTargetException {
        Optional<Method> first =
                Arrays.stream(implementation.getClass().getDeclaredMethods())
                        .filter(m -> m.getName().equals(message.getMethodName())).findFirst();

        if (message.getParameters().size() != message.getParametersStartingWith("arg").size()) {
            throw new RuntimeException("Parameter name format wrong");
        }

        if (!first.isPresent()) throw new RuntimeException("Couldn't find method");


        Method m = first.get();
        message.getParameters().keySet().forEach(System.out::println);
        int sizeOfMethocCallParams = (int) message.getParameters().keySet().stream()
                .map(k -> k.split("\\.")[0]).distinct().count();
        int sizeOfMethodParams = m.getParameterCount();

        if (sizeOfMethocCallParams != sizeOfMethodParams) throw new RuntimeException("Parameter count don't match");

        Object returnedObject = null;
        if (sizeOfMethodParams > 0) {
            Object[] newParameters = new Object[sizeOfMethodParams];
            for (int i = 0; i < sizeOfMethocCallParams; i++) {
                Map<String, String> parametersStartingWith = message.getParametersStartingWith("arg" + i);
                Object object = null;
                if (parametersStartingWith.size() > 1) {
                    Class<?> returnType = m.getParameterTypes()[i];
                    Field[] fields = returnType.getDeclaredFields();
                    Object instance = null;
                    try {
                        System.out.println(returnType);
                        instance = returnType.getDeclaredConstructor().newInstance();
                    } catch (InstantiationException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    for (Map.Entry<String, String> stringStringEntry : parametersStartingWith.entrySet()) {
                        String paramName = stringStringEntry.getKey().split("\\.")[1];
                        System.out.println(paramName);
                        Arrays.stream(fields).forEach(System.out::println);
                        Optional<Field> field = Arrays.stream(fields).filter(f -> f.getName().equals(paramName)).findFirst();
                        if (field.isPresent()) {
                            System.out.println("present");
                            Field field1 = field.get();
                            field1.setAccessible(true);
                            field1.set(instance, parseToCorretObject(field1.getType(), stringStringEntry.getValue()));
                        }

                    }
                    object = instance;
                } else {
                    object = parseToCorretObject(m.getParameterTypes()[i], parametersStartingWith.get("arg" + i));
                }
                newParameters[i] = object;
            }
            returnedObject = m.invoke(implementation, newParameters);
        } else {
            returnedObject = m.invoke(implementation);
            System.out.println("Invoked method: " + m);
        }

        System.out.println(returnedObject);
        if (returnedObject != null) {
            MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "result");
            if (parameterTypes.contains(returnedObject.getClass())) {
                methodCallMessage.setParameter("result", returnedObject.toString());
            } else {
                Field[] declaredFields = returnedObject.getClass().getDeclaredFields();
                for (Field field : declaredFields) {
                    field.setAccessible(true);
                    methodCallMessage.setParameter("result." + field.getName(), field.get(returnedObject).toString());
                }
            }
            messageManager.send(methodCallMessage, message.getOriginator());
        } else {
            sendEmptyReply(message.getOriginator());
        }


    }

    private Object parseToCorretObject(Class<?> returnType, String value) {
        if (String.class.equals(returnType)) {
            System.out.println("Returning string");
            return value;
        } else if (Integer.class.equals(returnType) || int.class.equals(returnType)) {
            System.out.println("Returning integer");
            return Integer.parseInt(value);
        } else if (char.class.equals(returnType)) {
            System.out.println("Returning char");
            return value.charAt(0);
        } else if (boolean.class.equals(returnType)) {
            System.out.println("Returning boolean");
            return Boolean.parseBoolean(value);
        } else if (double.class.equals(returnType)) {
            return Double.parseDouble(value);
        }

        return null;
    }


    private void sendEmptyReply(NetworkAddress address) {
        System.out.println("Sending empty reply");
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "result");
        methodCallMessage.setParameter("result", "Ok");
        messageManager.send(methodCallMessage, address);
    }

    @Override
    public void run() {
        while (true) {
            MethodCallMessage methodCallMessage = messageManager.wReceive();
            System.out.println(methodCallMessage);
            try {
                long l = System.currentTimeMillis();
                handleRequest(methodCallMessage);
                System.out.println(System.currentTimeMillis() - l);
            } catch (IllegalAccessException | InvocationTargetException e) {
                System.err.println(e.getMessage());
            }
        }

    }
}
